import os

from django.core.urlresolvers import reverse
from django.shortcuts import render,get_object_or_404,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.utils import timezone
from django.template.loader import render_to_string
from django.core.files.storage import default_storage

from .models import Slide

# Create your views here.
path = os.path.dirname(os.path.abspath(__file__)) # return this file's path (Ex. ~/wordbank/wordlists)
splited_path = path.split('/')
rstpath = '/'.join(splited_path[:-1])+'/rstfiles/' # cd to /csvfile (Ex. ~/wordbank/csvfile)
slidepath = '/'.join(splited_path[:-1])+'/rstconv/templates/slide_site/'
filepath = rstpath

def home_page(request):
	
	context = {}
	namelist = []
	# slidelist = []
	for file in os.listdir(rstpath):
		filename,ext = os.path.splitext(file)
		if ext == '.rst':
			namelist.append(filename)
	context['filelist'] = namelist
	''' # no longer use since have database
	for file in os.listdir(slidepath):
		filename,ext = os.path.splitext(file)
		if ext == '.html':
			slidelist.append(filename+ext)
	context['slides'] = slidelist
	'''
	context['slides'] = Slide.objects.all()

	return render(request, 'home.html',context)

def detail(request, rstfile):
	slide = get_object_or_404(Slide,slide_name=rstfile)
	slide.view_count = slide.view_count + 1
	slide.save()

	return render(request,'slide_site/'+rstfile+'.html') 
	

def rst2html5(request):
	if request.method == 'POST':
		filepath = rstpath+request.POST['rstfile']+'.rst'
		slide_name = request.POST['slide_name']+'.html'
		slidestyle = request.POST['slidestyle']

		newslide = Slide(slide_name=request.POST['slide_name'],pub_date=timezone.now(),view_count=0) 
		newslide.save()

		if(slidestyle == 'clean'):
			os.system("rst2html5 "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'deck'):
			os.system("rst2html5 --deck-js --pretty-print-code --embed-content "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'reveal'):
			os.system("rst2html5 --jquery --reveal-js --pretty-print-code "+filepath+" > "+slidepath+slide_name)
		
		elif(slidestyle == 'bootstrap'):
			os.system("rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'pygments'):
			os.system("rst2html5 --pygments "+filepath+" > "+slidepath+slide_name)
	
	return HttpResponseRedirect('/')

def newslide(request):
	if request.method == 'POST':
		image = request.FILES['image']
		savepath = default_storage.save('rstconv/static/pictures/'+str(image),image)

	return render(request,'newslide.html')

def text2rst(request):
	if request.method == 'POST':
		text = request.POST['text']
		rst_name = request.POST['slide_name']+'.rst'
		slide_name = request.POST['slide_name']+'.html'
		slidestyle = request.POST['slidestyle']
		filepath = rstpath+rst_name

		with open(filepath,'w') as rst_file:
			rst_file.write(text)
			rst_file.close()

		newslide = Slide(slide_name=request.POST['slide_name'],pub_date=timezone.now(),view_count=0) 
		newslide.save()
		
		### should call rst2html5 function instead
		if(slidestyle == 'clean'):
			os.system("rst2html5 "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'deck'):
			os.system("rst2html5 --deck-js --pretty-print-code --embed-content "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'reveal'):
			os.system("rst2html5 --jquery --reveal-js --pretty-print-code "+filepath+" > "+slidepath+slide_name)

		elif(slidestyle == 'bootstrap'):
			os.system("rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content "+filepath+" > "+slidepath+slide_name)
		elif(slidestyle == 'pygments'):
			os.system("rst2html5 --pygments "+filepath+" > "+slidepath+slide_name)
		###
		

	return HttpResponseRedirect('/')











