from django.conf.urls import url

from . import views

app_name = 'rstconv'
urlpatterns = [
	url(r'^$', views.home_page, name='home'),
	url(r'^detail/(?P<rstfile>[a-z,A-Z,0-9,\_]+)', views.detail, name='detail'),
	url(r'^rst2html5/', views.rst2html5, name='rst2html5'),
	url(r'^newslide/', views.newslide, name='newslide'),
	url(r'^text2rst/', views.text2rst, name='text2rst'),
]
