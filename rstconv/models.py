from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone

# Create your models here.

class Slide(models.Model):
	slide_name = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	view_count = models.IntegerField(default=0)

	def __str__(self):
		return self.slide_name
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
